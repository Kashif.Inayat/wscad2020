#!/bin/bash

set -ex

mkdir -p build/conv-def-b
mkdir -p build/conv-def-i

seq 1 10 | parallel "make out_dir=./build/conv-def-b/ EXEC=conv-def-b{} BATCH_SIZE={}"
seq 10 10 100 | parallel "make out_dir=./build/conv-def-i/ EXEC=conv-def-i{} IN_DIM={}"
