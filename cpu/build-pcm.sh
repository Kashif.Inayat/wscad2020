#!/bin/bash

set -ex

git submodule update --init pcm
cd pcm
make
cd ./..
mkdir -p lib
cp pcm/lib* lib/
