#include <stdint.h>
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
//#include "include/gemmini_testutils.h"
#include "cpucounters.h"
#include <iostream>

#ifndef BATCH_SIZE
#define BATCH_SIZE 3
#endif
#ifndef IN_DIM
#define IN_DIM 23
#endif
#ifndef IN_CHANNELS
#define IN_CHANNELS 17
#endif
#ifndef OUT_CHANNELS
#define OUT_CHANNELS 31
#endif
#ifndef KERNEL_DIM
#define KERNEL_DIM 3
#endif
#ifndef PADDING
#define PADDING 1
#endif
#ifndef STRIDE
#define STRIDE 2
#endif

#define NO_BIAS false

#define OUT_DIM ((IN_DIM + 2*PADDING - KERNEL_DIM) / STRIDE + 1)
#define PATCH_SIZE (KERNEL_DIM * KERNEL_DIM * IN_CHANNELS)
#define N_PATCHES (BATCH_SIZE * OUT_DIM * OUT_DIM)

#include <stdint.h>
#include <stdbool.h>

#define ROUNDING_RIGHT_SHIFT(x, shift) \
    ((x) / (1 << (shift)))
typedef int8_t elem_t;
elem_t elem_t_max = 127;
elem_t elem_t_min = -128;
typedef int32_t acc_t;
typedef int64_t full_t;

void conv(int batch_size, int in_channels, int in_dim,
        int out_channels, int kernel_dim,
        int out_dim,
        int stride, int padding,
        elem_t input[BATCH_SIZE][IN_DIM][IN_DIM][IN_CHANNELS],
        elem_t weights[OUT_CHANNELS][KERNEL_DIM][KERNEL_DIM][IN_CHANNELS],
        acc_t bias[OUT_CHANNELS],
        elem_t output[BATCH_SIZE][OUT_DIM][OUT_DIM][OUT_CHANNELS]) {

    for (int b = 0; b < batch_size; b++) {
        for (int orow = 0; orow < out_dim; orow++) {
            for (int ocol = 0; ocol < out_dim; ocol++) {
                for (int och = 0; och < out_channels; och++) {
                    acc_t result = bias[och];

                    for (int krow = 0; krow < kernel_dim; krow++) {
                        for (int kcol = 0; kcol < kernel_dim; kcol++) {
                            for (int kch = 0; kch < in_channels; kch++) {
                                int irow = orow * stride + krow - padding;
                                int icol = ocol * stride + kcol - padding;

                                elem_t pixel = irow < 0 || irow >= in_dim ||
                                    icol < 0 || icol >= in_dim ?
                                    0 : input[b][irow][icol][kch];

                                result +=
                                    weights[och][krow][kcol][kch] *
                                    pixel;
                            }
                        }
                    }

                    // Shift while rounding to nearest integer (ties round to negative infinity)
                    result = ROUNDING_RIGHT_SHIFT(result, 0);

                    // Clip result
                    result = result > elem_t_max ? elem_t_max : (result < elem_t_min ? elem_t_min : result);

                    output[b][orow][ocol][och] = result;
                }
            }
        }
    }
}
/*
void conv2(int batch_size, int in_channels, int in_dim,
        int out_channels, int kernel_dim,
        int out_dim,
        int stride, int padding,
        elem_t *input,
        elem_t *weights,
        acc_t *bias,
        elem_t *output) {

    for (int b = 0; b < batch_size; b++) {
        for (int orow = 0; orow < out_dim; orow++) {
            for (int ocol = 0; ocol < out_dim; ocol++) {
                for (int och = 0; och < out_channels; och++) {
                    acc_t result = bias[och];

                    for (int krow = 0; krow < kernel_dim; krow++) {
                        for (int kcol = 0; kcol < kernel_dim; kcol++) {
                            for (int kch = 0; kch < in_channels; kch++) {
                                int irow = orow * stride + krow - padding;
                                int icol = ocol * stride + kcol - padding;

                                elem_t pixel = irow < 0 || irow >= in_dim ||
                                    icol < 0 || icol >= in_dim ?
                                    0 : input[b][irow][icol][kch];

                                result +=
                                    weights[och][krow][kcol][kch] *
                                    pixel;
                            }
                        }
                    }

                    // Shift while rounding to nearest integer (ties round to negative infinity)
                    result = ROUNDING_RIGHT_SHIFT(result, 0);

                    // Clip result
                    result = result > elem_t_max ? elem_t_max : (result < elem_t_min ? elem_t_min : result);

                    output[b*orow*ocol*och + orow*ocol*och + ocol*och + och] = result;
                }
            }
        }
    }
}
*/
bool vec_is_equal(elem_t * a, elem_t * b, int len) {
    for (int i = 0; i < len; i++)
        if (a[i] != b[i])
            return false;
    return true;
}

void init_random(elem_t * buf, int len) {
    for (elem_t * ptr = buf; ptr < buf + len; ptr++) {
        // *ptr = (rand() % 32) - 16;
        *ptr = (rand() % 5) - 2;
    }
}

void init_random_acc(acc_t * buf, int len) {
    for (acc_t * ptr = buf; ptr < buf + len; ptr++) {
        // *ptr = (rand() % 32) - 16;
        *ptr = (rand() % 5) - 2;
    }
}

void init_zeros_acc(acc_t * buf, int len) {
    for (acc_t * ptr = buf; ptr < buf + len; ptr++) {
        *ptr = 0;
    }
}

using namespace pcm;
int main(int argc, char* argv[]) {
    PCM *m = PCM::getInstance();
    m->program();

    ServerUncoreCounterState * BeforeState = new ServerUncoreCounterState[m->getNumSockets()];
    ServerUncoreCounterState * AfterState = new ServerUncoreCounterState[m->getNumSockets()];
    for (uint64_t i = 0; i < m->getNumSockets(); ++i)
        BeforeState[i] = m->getServerUncoreCounterState(i);

    std::vector<CoreCounterState> cstates1, cstates2;
    std::vector<SocketCounterState> sktstate1, sktstate2;
    SystemCounterState sstate1, sstate2;
    m->getAllCounterStates(sstate1, sktstate1, cstates1);

    //printf("Output dimension: %u\n\n", OUT_DIM);

    static elem_t input[BATCH_SIZE][IN_DIM][IN_DIM][IN_CHANNELS];
    static elem_t weights[OUT_CHANNELS][KERNEL_DIM][KERNEL_DIM][IN_CHANNELS];
    static acc_t bias[OUT_CHANNELS];
    static elem_t output[BATCH_SIZE][OUT_DIM][OUT_DIM][OUT_CHANNELS];

//    printf("Randomize inputs...\n");
    init_random(&input[0][0][0][0], sizeof(input) / sizeof(elem_t));

//    printf("Randomize weights...\n");
    init_random(&weights[0][0][0][0], sizeof(weights) / sizeof(elem_t));

//    printf("Randomize bias...\n");
    if (NO_BIAS)
        init_zeros_acc(&bias[0], sizeof(bias) / sizeof(acc_t));
    else
        init_random_acc(&bias[0], sizeof(bias) / sizeof(acc_t));


//    printf("CPU conv...\n");
    //uint64_t start_cpu = read_cycles();
    conv(BATCH_SIZE, IN_CHANNELS, IN_DIM,
            OUT_CHANNELS, KERNEL_DIM,
            OUT_DIM,
            STRIDE, PADDING,
            input,
            weights,
            bias,
            output);

    //uint64_t end_cpu = read_cycles();
    //printf("CPU conv took %llu cycles\n", end_cpu - start_cpu);

    for (uint64_t i = 0; i < m->getNumSockets(); ++i)
        AfterState[i] = m->getServerUncoreCounterState(i);

    m->getAllCounterStates(sstate2, sktstate2, cstates2);

    std::cout << "Energy: " << getConsumedJoules(BeforeState[0], AfterState[0]) << std::endl;
    uint64_t cycles = getCycles(sstate1, sstate2);
    uint64_t insts = getInstructionsRetired(sstate1, sstate2);
    std::cout << "Cycles: " << cycles << std::endl;
    std::cout << "Instructions: " << insts << std::endl;
//    printf("Finished!\n");
    return 0;
}
