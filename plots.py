#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import os
import pathlib
import glob
from scipy.stats.mstats import gmean
#plt.style.use('ggplot')

plt.rcParams.update({'font.size': 13})

# Default values
TYPE_SIZE = 1
BATCH_SIZE = 4
IN_DIM = 56
IN_CHANNELS = 17
OUT_CHANNELS = 31
KERNEL_DIM = 3
PADDING = 1
STRIDE = 2

CPU_FREQ = 3.0 * 10**9
GEM_FREQ = 500 * 10**6
GEM_SYS_PWR = 611 * 0.93 * 10**-3 # Watts

pathlib.Path("./plots").mkdir(parents=True, exist_ok=True)

def readReport(path, basename):
    f = open(path, "r")
    energies = []
    #temps = []
    times = []
    for line in f:
        #if "Package id" in line:
        #    words = line.split()
        #    temps.append(float(words[3][1:4]))
        if "Energy:" in line:
            words = line.split()
            energies.append(float(words[1]))
        if "real:" in line:
            words = line.split()
            times.append(float(words[1]))

    f.close()
    return np.array(energies), np.array(times)

def readReportsDir(path):
    files = glob.glob(path)
    energies = []
    #temps = []
    times = []
    for file in files:
        energy, time, = readReport(file, None)
        energies.append(energy)
        #temps.append(temp)
        times.append(time)

#    print(energies)
#    print(temps)
    energies = np.stack(energies)
    #temps = np.stack(temps)
    times = np.stack(times)

    energies = np.mean(energies, axis=0)
    #temps = np.mean(temps, axis=0)
    std_dev = np.std(times, axis=0)
    times = np.mean(times, axis=0)
    return energies, times, std_dev


def readCycles(path, base_name):
    f = open(path, "r")
    bench = []
    cycles = []
    for line in f:
        if "took" in line:
            words = line.split()
            cycles.append(int(words[3]))
        if base_name in line:
            line = os.path.basename(line)
            bench.append(int(line.replace(base_name, '')))

    f.close()
    bench, cycles = (list(t) for t in zip(*sorted(zip(bench, cycles))))
    return np.array(bench), np.array(cycles)

def createTimePlot(bench,
        xtitle='NONAME',
        y1lines=None,
        y1errors=None,
        y1labels=None,
        y1title='Time',
        y2lines=None,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        loc='lower right',
        annotate=False,
        path="fig.pdf"):

    fig, ax1 = plt.subplots()
    for i in range(len(y1lines)):
        if (y1errors is not None):
            if (y1errors[i] is not None):
                ax1.errorbar(bench, y1lines[i], yerr=y1errors[i], label=labels[i], marker='o')
        else:
            ax1.plot(bench, y1lines[i], label=labels[i], marker='o')
        if (i != 0):
            if (annotate == True):
                val = y1lines[i][-1] / y1lines[0][-1]
                text_x = bench[-1]
                text_y = y1lines[i][-1]*(1-0.08)
                ax1.annotate(str(round(val, 1))+'x', (bench[-1], y1lines[i][-1]), xytext=(text_x, text_y))

    ax1.set_xlabel(xtitle)
    ax1.set_ylabel(y1title)
    ax1.xaxis.set_major_locator(MaxNLocator(integer=True))

    # Axis 2
    if (y2lines is not None):
        color = 'tab:red'
        ax2 = ax1.twinx()
        ax2.plot(bench, y2lines, 'r--', label=y2label)
        ax2.set_ylabel(y2title, color=color)
        #ax2.tick_params(axis="y", labelcolor=color)
        ax2.tick_params(axis="y")
    else:
        ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)

    fig.tight_layout()
    if (loc == 'lower right'):
        fig.legend(loc=loc, bbox_to_anchor=(1,0), bbox_transform=ax1.transAxes)
    if (loc == 'upper right'):
        fig.legend(loc=loc, bbox_to_anchor=(1,1), bbox_transform=ax1.transAxes)
    if (loc == 'upper left'):
        fig.legend(loc=loc, bbox_to_anchor=(0,1), bbox_transform=ax1.transAxes)
    plt.savefig(path)
    plt.close()

def createTwoYPlot(bench,
        xtitle='NONAME',
        y1lines=None,
        y1labels=None,
        y1title='Time',
        y2lines=None,
        y2labels=None,
        y2title='Input Tensor (KB)',
        loc='lower right',
        path="fig.pdf"):

    fig, ax1 = plt.subplots()
    for i in range(len(y1lines)):
        ax1.plot(bench, y1lines[i], label=labels[i])
    #ax1.plot(bench, line1, label=line1_label)
    #ax1.plot(bench, line2, label=line2_label)
    ax1.set_xlabel(xtitle)
    ax1.set_ylabel(y1title)
    ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
    #plt.xticks(bench)

    # Axis 2
    if (y2lines is not None):
        color = 'tab:red'
        ax2 = ax1.twinx()
        for i in range(len(y2lines)):
            ax2.plot(bench, y2lines[i], '--', label=y2labels[i])
        ax2.set_ylabel(y2title, color=color)
        #ax2.tick_params(axis="y", labelcolor=color)
        ax2.tick_params(axis="y")

    fig.tight_layout()
    if (loc == 'lower right'):
        fig.legend(loc=loc, bbox_to_anchor=(1,0), bbox_transform=ax1.transAxes)
    if (loc == 'upper right'):
        fig.legend(loc=loc, bbox_to_anchor=(1,1), bbox_transform=ax1.transAxes)
    if (loc == 'upper left'):
        fig.legend(loc=loc, bbox_to_anchor=(0,1), bbox_transform=ax1.transAxes)
    plt.savefig(path)
    plt.close()

def createEnergyPlot(bench, 
        lines,
        labels,
        xlabel="NO LABEL",
        ylabel="Energy (J)",
        loc='upper left',
        path="fig.pdf"):
    for i in range(len(lines)):
        plt.plot(bench, lines[i], label=labels[i], marker='o')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(loc=loc)
    plt.gca().spines['right'].set_visible(False)
    plt.gca().spines['top'].set_visible(False)
    plt.savefig(path)
    plt.close()


# Collect Gemmini data
res_dir = './riscv/results'

benches_b, gem_cycles_b = readCycles(res_dir+"/conv-def-b.txt", "conv-def-b")
gem_times_b = gem_cycles_b / GEM_FREQ
sizes_b = benches_b * IN_DIM * IN_DIM * IN_CHANNELS * TYPE_SIZE / 1024

benches_i, gem_cycles_i = readCycles(res_dir+"/conv-def-in-dim.txt", "conv-def-in-dim")
gem_times_i = gem_cycles_i / GEM_FREQ
sizes_i = BATCH_SIZE * benches_i * benches_i * IN_CHANNELS * TYPE_SIZE / 1024

# Collect CPU data
res_dir = './cpu/results'
cpu1_energies_b, cpu1_times_b, cpu1_std_dev_b = readReportsDir(res_dir+"/conv-def-b/cpu1/*.txt")
cpu1_energies_i, cpu1_times_i, cpu1_std_dev_i = readReportsDir(res_dir+"/conv-def-i/cpu1/*.txt")
#cpu_energies_b_i, cpu_times_b_i = readReportsDir(res_dir+"/conv-def-b-i/*.txt")
#in_dim = np.arange(10, 101, 10)
#b_sizes = np.arange(1, 11)
#sizes_b_i = b_sizes * in_dim * in_dim * IN_CHANNELS * TYPE_SIZE / 1024
cpu2_energies_b, cpu2_times_b, cpu2_std_dev_b = readReportsDir(res_dir+"/conv-def-b/cpu2/*.txt")
cpu2_energies_i, cpu2_times_i, cpu2_std_dev_i = readReportsDir(res_dir+"/conv-def-i/cpu2/*.txt")

# Create plots
# Plots separated
lines = [gem_times_b]
labels = ['Gemmini']
createTimePlot(benches_b,
        y1lines=lines,
        y1labels=labels,
        xtitle='Batch Size',
        y1title='Time (s)',
        y2lines=sizes_b,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        path="plots/gem_batch.pdf")
lines = [gem_times_i]
labels = ['Gemmini']
createTimePlot(benches_i,
        y1lines=lines,
        y1labels=labels,
        xtitle='In Dim Size',
        y1title='Time (s)',
        y2lines=sizes_i,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        path="plots/gem_in_dim.pdf")

lines = [cpu1_times_b, cpu2_times_b]
labels = ['CPU-LL', 'CPU-HL']
createTimePlot(benches_b,
        y1lines=lines,
        y1labels=labels,
        y1errors=[cpu1_std_dev_b, cpu2_std_dev_b],
        xtitle='Batch Size',
        y1title='Time (s)',
        y2lines=sizes_b,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        path="plots/cpu_batch.pdf")
lines = [cpu1_times_i, cpu2_times_i]
labels = ['CPU-LL', 'CPU-HL']
createTimePlot(benches_i,
        y1lines=lines,
        y1labels=labels,
        y1errors=[cpu1_std_dev_i, cpu2_std_dev_i],
        xtitle='In Dim Size',
        y1title='Time (s)',
        y2lines=sizes_i,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        path="plots/cpu_in_dim.pdf")


# Plots together
lines = [gem_times_b, cpu1_times_b, cpu2_times_b]
labels = ['Gemmini', 'CPU-LL', 'CPU-HL']

createTimePlot(benches_b,
        y1lines=lines,
        y1labels=labels,
        xtitle='Batch Size',
        y1title='Time (s)',
        y2lines=None,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        loc='upper left',
        annotate=True,
        path="plots/batch.pdf")

lines = [gem_times_i, cpu1_times_i, cpu2_times_i]

createTimePlot(benches_i,
        y1lines=lines,
        y1labels=labels,
        xtitle='In Dim Size',
        y1title='Time (s)',
        y2lines=None,
        y2title='Input Tensor (KB)',
        y2label='Input size',
        loc='upper left',
        annotate=True,
        path="plots/in_dim.pdf")


# Energy Plots
gem_energies_b = GEM_SYS_PWR * gem_times_b
gem_energies_i = GEM_SYS_PWR * gem_times_i

lines = [gem_energies_b, cpu1_energies_b, cpu2_energies_b]
labels = ['Gemmini', 'CPU-LL', 'CPU-HL']
efficiency = [cpu1_energies_b / gem_energies_b, cpu2_energies_b / gem_energies_b]
labels2 = ['LL/GEM', 'HL/GEM']
createEnergyPlot(benches_b, 
    lines,
    labels,
    xlabel="Batch Size",
    ylabel="Energy (J)",
    path="plots/batch_energy.pdf")
'''
createTwoYPlot(benches_b,
    xtitle='Batch Size',
    y1lines=lines,
    y1labels=labels,
    y1title='Energy (J)',
    y2lines=efficiency,
    y2labels=labels2,
    y2title='Efficiency',
    loc='lower right',
    path="plots/batch_energy.pdf"
    )
'''
lines = [gem_energies_i, cpu1_energies_i, cpu2_energies_i]
labels = ['Gemmini', 'CPU-LL', 'CPU-HL']
efficiency = [cpu1_energies_i / gem_energies_i, cpu2_energies_i / gem_energies_i]
'''
createTwoYPlot(benches_i,
    xtitle='In Dim Size',
    y1lines=lines,
    y1labels=labels,
    y1title='Energy (J)',
    y2lines=efficiency,
    y2labels=labels2,
    y2title='Efficiency',
    loc='lower right',
    path="plots/in_dim_energy.pdf"
    )
'''

createEnergyPlot(benches_i, 
    lines,
    labels,
    xlabel="In Dim Size",
    ylabel="Energy (J)",
    path="plots/in_dim_energy.pdf")

# Frequency Plots #
lines = [gem_cycles_b / cpu1_times_b / 10**6,
         gem_cycles_b / cpu2_times_b / 10**6]
labels = ['CPU-LL', 'CPU-HL']
createTimePlot(benches_b,
        y1lines=lines,
        y1labels=labels,
        xtitle='Batch Size',
        y1title='Minimum Frequency (MHz)',
        y2lines=None,
        y2title=None,
        y2label=None,
        loc='upper right',
        path="plots/batch-min-freq.pdf")

lines = [gem_cycles_i / cpu1_times_i / 10**6,
         gem_cycles_i / cpu2_times_i / 10**6]
labels = ['CPU-LL', 'CPU-HL']
createTimePlot(benches_i,
        y1lines=lines,
        y1labels=labels,
        xtitle='In Dim Size',
        y1title='Minimum Frequency (MHz)',
        y2lines=None,
        y2title=None,
        y2label=None,
        loc='upper right',
        path="plots/input-min-freq.pdf")

# General results

print("--- Time results ---")
print("Batch time results")
print("CPU-LL/GEMM: ", cpu1_times_b/gem_times_b)
print("CPU-HL/GEMM: ", cpu2_times_b/gem_times_b)

print("In Dim time results")
print("CPU-LL/GEMM: ", cpu1_times_i/gem_times_i)
print("CPU-HL/GEMM: ", cpu2_times_i/gem_times_i)


print("--- Energy results ---")
print("Batch energy results")
print("CPU-LL/GEMM: ", cpu1_energies_b/gem_energies_b)
print("CPU-HL/GEMM: ", cpu2_energies_b/gem_energies_b)

print("In Dim energy results")
print("CPU-LL/GEMM: ", cpu1_energies_i/gem_energies_i)
print("CPU-HL/GEMM: ", cpu2_energies_i/gem_energies_i)

print("--- Frequency results ---")
print("Batch frequency results")
print("CPU-LL/GEM: ", gem_cycles_b / cpu1_times_b / 10**6)
print("CPU-HL/GEM: ", gem_cycles_b / cpu2_times_b / 10**6)

print("In dim frequency results")
print("CPU-LL/GEM: ", gem_cycles_i / cpu1_times_i / 10**6)
print("CPU-HL/GEM: ", gem_cycles_i / cpu2_times_i / 10**6)

print("--- Sizes ---")
print("Batch sizes: ", sizes_b)
print("In Dim Sizes: ", sizes_i)