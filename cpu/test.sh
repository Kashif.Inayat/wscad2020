#!/bin/bash

set -e

rm -rf results

sudo modprobe msr

mkdir -p results/conv-def-b/cpu1
mkdir -p results/conv-def-i/cpu1

mkdir -p results/conv-def-b/cpu2
mkdir -p results/conv-def-i/cpu2

stress_begin () {
	stress -c $(expr $(nproc) - 1) &
}

stress_end () {
	killall stress
}

test () {
	for bench in $1
	do
		echo "Start $bench" &>> $2
		sensors &>> $2
		\time -f "real: %e" $bench &>> $2
		echo "------------" &>> $2
	done
}

b1=$(ls -d $PWD/build/conv-def-b/* | sort -V)
b2=$(ls -d $PWD/build/conv-def-i/* | sort -V)

# Tests without stress.
for i in {1..500}
do
	echo $i
	test "$b1" results/conv-def-b/cpu1/$i.txt
	test "$b2" results/conv-def-i/cpu1/$i.txt
done

# Tests with stress.
stress_begin
sleep 50
for i in {1..500}
do
	echo $i
	test "$b1" results/conv-def-b/cpu2/$i.txt
	test "$b2" results/conv-def-i/cpu2/$i.txt
done
stress_end
